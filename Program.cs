﻿using System;

namespace zadatak1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Note note1 = new Note("Movies", "Inception, The Godfather, In Time");
            Note note2 = new Note("TV Shows", "Breaking Bad, The Sopranos, The Wire");
            Note note3 = new Note("Male names", "Marco, Roger, Novak");
            Note note4 = new Note("Female names", "Serena, Venus, Donna");

            Notebook notebook = new Notebook();

            notebook.AddNote(note1);
            notebook.AddNote(note2);
            notebook.AddNote(note3);
            notebook.AddNote(note4);
            notebook.RemoveNote(note3);
            notebook.RemoveNote(note2);

            IAbstractIterator iterator = notebook.GetIterator();

            while (!iterator.IsDone)
            {
                iterator.Current.Show();
                iterator.Next();
            }*/
            ////////////////////drugi
            Product prod1 = new Product("Jeans Trousers", 38.99);
            Product prod2 = new Product("Hilfiger T-Shirt", 20.00);
            Product prod3 = new Product("Nike T-Shirt", 15.00);

            Box box = new Box();

            box.AddProduct(prod1);
            box.AddProduct(prod2);
            box.AddProduct(prod3);
            box.RemoveProduct(prod2);

            IAbstractIterator iterator = box.GetIterator();

            while (!iterator.IsDone)
            {
                Console.WriteLine(iterator.Current);
                iterator.Next();
            }

        }

    }
}