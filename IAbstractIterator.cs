﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zadatak1
{
    interface IAbstractIterator
    {
        Product First();                //zad1 = Note First();
        Product Next();                 //zad1 = Note Next();
        bool IsDone { get; }
        Product Current { get; }        //zad1 = Note Current { get; }
    }
}
